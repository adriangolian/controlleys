# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a samples controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
## - call exposes all registered services (none by default)
#########################################################################
from gluon.contrib.login_methods.gae_google_account import GaeGoogleAccount
auth.settings.login_form = GaeGoogleAccount()

response.meta.keywords='todo, to-do, list, app, task, control'
response.meta.description='To-do list app'
response.meta.author='Adrian Golian adriangolian@yahoo.com'

def index():
    response.title="Controlleys - the to-do app"

    return locals()


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())


def download():

    return response.download(request, db)


def call():

    return service()


@auth.requires_signature()
def data():
    return dict(form=crud())

@auth.requires_login()    
def display_your_form():
    update = db.tasks(request.args(0))
    form = SQLFORM(db.tasks, update)
    if form.accepts(request,session):
        response.flash = 'Thanks! The form has been submitted.'
    elif form.errors:
       response.flash = 'Please correct the error(s).'
    else:
       response.flash = 'Try again - no fields can be empty.'
    return dict(form=form)
   
@auth.requires_login()
def controlley():
    response.title="Your Controlley"
    db.tasks.id.readable = False
    grid = SQLFORM.grid(db.tasks.owner_id==auth.user.id, user_signature=False)
    return dict(grid=grid)
